<?php

//ini_set('display_errors', 1);
//error_reporting(E_ALL);

include_once "scripts/users_errors.php";
set_error_handler("usersError");

include "index.html";

session_start();

if(isset($_GET['reg_page']))
{
    include "scripts/registration.php";
    include "scripts/registration.html";
}
else if (isset($_SESSION['name']))
{
    include "scripts/diary.php";
    include "scripts/diary.html";
}
else
{
    include "scripts/login.html";
    include "scripts/login.php";    
}

include "footer.html";