<?php
require "FamilyMember.php";

//include "../index.html";
//include "registration.html";
//include "../footer.html";


//include_once "users_errors.php";
//set_error_handler("usersError");


if (!isset($_POST['submit'])) return;

if (checkFields())
{  
    $name = htmlentities(trim($_POST['name']));
        
    $password = htmlentities(trim($_POST['password']));
        
    $member = $_POST['member'];
       
    $guest = new Guest($name, $password, $member); 
    
    if ($guest->register()) drawState("Register success");

}
else trigger_error("Fill all the fields", E_USER_NOTICE);
                  
 
function checkFields()
{
    if(!empty($_POST['name']) && !empty($_POST['password']) && !empty($_POST['member'])) return true;
    else  return false;
}

function drawState($str)
{
     echo <<<STATE
        <script>
            document.getElementById('errors').innerHTML = "$str";
        </script>
STATE;
}


