<?php

//ini_set('display_errors', 1);
//error_reporting(E_ALL);

require "FamilyMember.php";
    

if (isset($_POST['submit']))
{
    $name = htmlentities(trim($_POST['name']));
    $password = htmlentities(trim($_POST['password']));

    if (empty($name)) echo "input your name \n";
    elseif (empty($password)) echo "input your password \n";
    else
    {
        $guest = new Guest($name, $password);   

        $login_status = $guest->login();

        if($login_status == "OK"){           
            header('Location: ' . $_SERVER['PHP_SELF']);
        }
        else trigger_error($login_status, E_USER_WARNING);
    }
}

