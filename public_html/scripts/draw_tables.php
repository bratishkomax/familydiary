<?php

function drawViewTable($result)
{
    $str_start = <<<START
        <div class="content">    
            <table>
                <tr>
                    <th width="70%">Task</th>
                    <th width="15%">Mark done</th>
                    <th width="15%">Save</th>
                </tr>
START;
        
    $str = "";
        
    while($obj = $result->fetch_object()) {
            
        $str_chk_box = "";
        if($obj->mark) $str_chk_box = "checked";
            
        $str .= <<<MAIN
            <tr>
            <form action="" method="post">
                <td>$obj->task</td>
                <td><input type="checkbox" name="chbox" value ="$obj->id" $str_chk_box></td>
                <td><input type="submit" name="mark_task" value="Save"></td>
                <input type="hidden" name="chbox_id" value="$obj->id">
            </form>
            </tr>
MAIN;
        }
        
        $str_end = <<<END
            </table>
        </div>
END;
        echo $str_start . $str . $str_end;
    
}



function drawDistrTable($tasks, $names)
{      
    $i = 0;
    $arr_names = [];
    while($obj = $names->fetch_object()) {
        $arr_names[$i] = $obj->name;
        $i++;
    }       
    
        $str_start = <<<START
        <div class="content">    
            <table>
                <tr>
                    <th width="20%">Name</th>
                    <th width="80%">Task</th>
                </tr>
START;
        
        $str = "";
        
        while($obj = $tasks->fetch_object()) {
            
            $str_select = "";
            foreach($arr_names as $name) {
                if ($name == $obj->name) continue;
                $str_select .= "<option value=\"$name\">$name</option> " ;

            }
            
            $str .= <<<MAIN
            <tr>
                <td>
                    <select name="member" class="opt" id="$obj->id">
                        <option value="$obj->name">$obj->name</option>
                       $str_select
                    </select>
                </td>
                <td>$obj->task</td>
            </tr>
MAIN;
        }
        
        $str_end = <<<END
        </table>
            <div>
                <input type="button" name="distrib" class="btns" id="btn_save_tasks" value="Save"></br>
                <p><input type="button" class="btns" value="Back" onclick="history.back()"> </p>
                <div id="status"></div>
            </div>
        </div>
            
END;
        echo $str_start . $str . $str_end;
    
}

function drawEmptyContent($str)
{
    $str = <<<BODY
        <div  class="content">
            <span>$str</span>
        </div>
BODY;
    echo $str;
}
