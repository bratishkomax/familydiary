<?php

require "FamilyMember.php";

if(!isset($_SESSION['name']) || !isset($_SESSION['member']))
    trigger_error("Could not start sassion", E_USER_ERROR);

$className = $_SESSION['member'];
$user = new $className ($_SESSION['name'], $_SESSION['member']);
            

if(isset($_POST["upload_file"])) {
    
    $user->loadTasks("home_tasks");
}
    
if(isset($_POST['mark_task'])) {
    $id = $_POST['chbox_id'];
    $mark = $_POST['chbox']?1:0;
    
    if(!$user->updateTable($id, $mark)) trigger_error("Could not upadate 'tasks' table", E_USER_ERROR);
    else header('Location: ' . $_SERVER['PHP_SELF']);
}

if (isset($_POST['logout'])) {
    session_destroy();
    header('Location: ' . $_SERVER['PHP_SELF']);
}

include_once "draw_tables.php";
if (isset($_POST['distribute_tasks'])) {
    if(!($tasks = $user->getAllTasks()) || !($names = $user->getAllNames()))drawEmptyContent("No loaded tasks");
    else drawDistrTable($tasks, $names);
}
else
{    
    if (!$tasks = $user->getTasks()) drawEmptyContent("Tasks is not disribute");
    else drawViewTable($tasks);
}
    
if (isset($_POST['jstr'])) {
   $data = json_decode($_POST['jstr'], true);
	    
    if(!$user->distribute_tasks($data)) trigger_error("Could not upadate 'tasks' table", E_USER_ERROR);
    else echo "updating success";
        header('Location: ' . $_SERVER['PHP_SELF']);
     echo "updating success";
}

    
?>