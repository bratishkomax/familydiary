<?php

//ini_set('display_errors', 1);
//error_reporting(E_ALL);

abstract class FamilyMember
{
	protected $name;
	protected $member;
    protected $password_hash;
    protected $mysqli;
    private $id;
	
	function __construct($name, $member)
	{	        
		$this->name = $name;
		$this->member = $member;
        
       try {
           $this->mysqli = $this->newDBConnect();
       }
        catch(Exception $e) {
            echo trigger_error($e->getMessage(), E_USER_ERROR);
        }            
    }
    
    function __destruct()
    {
        $this->mysqli->close();
    }

	public function getName()
	{
		return $this->name;
	}
    public function getMember()
	{
		return $this->member;
	}
	public function getId()
	{
		return $this->id;
	}
    public function getPasswordHash()
	{
		return $this->password_hash;
	}
    
    private function newDBConnect()
    {
       $mysqli = @new mysqli("testphp.com", "root", "root", "FamilyDB");
       if ($mysqli->connect_errno) {
          throw new Exception("Could not connect to MySQL database ");
       }
        return $mysqli;
    }
    
    protected function userExist()
    {
        $result = $this->mysqli->query("SELECT name FROM users WHERE name = \"$this->name\"");
        if ($result->fetch_assoc()) return true;

        return false;
    }
    
    protected function createNewTable($table)
    {
        switch($table){
                
            case 'users':
                $result = $this->mysqli->query("CREATE TABLE IF NOT EXISTS users(
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    name VARCHAR(20) NOT NULL,
                    member  VARCHAR(10) NOT NULL,
                    password  VARCHAR(255) NOT NULL)");
                break;
                
            case 'tasks':
                $result = $this->mysqli->query("CREATE TABLE IF NOT EXISTS tasks(
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    name VARCHAR(20) NOT NULL,
                    task  VARCHAR(200) NOT NULL,
                    mark BOOLEAN NOT NULL)");
                break;
                
            default:
                trigger_error("Trying create table with wrong name", E_USER_ERROR);
                return "wrong table name";
                break;
        }
        return $result;
    }
    
    protected function tableExist($table)
    {        
        $result = $this->mysqli->query("SHOW TABLES LIKE '$table'");
        
        if($result->num_rows > 0) return true;
        else return false;
    }
    
    public function getTasks()
    {
        if (!$this->tableExist('tasks')) return false;
           
        $result = $this->mysqli->query("SELECT * FROM tasks WHERE name = \"$this->name\"");
        
        if($result->num_rows > 0) return $result;
        else return false;
    }
    
    public function getAllTasks()
    {
        return "no permissions";
    }
    public function loadTasks($file_name)
    {
        return "no permissions";
    }
    
    public function updateTable($id, $mark)
    {
        $result = $this->mysqli->query("UPDATE tasks SET mark = $mark WHERE id = $id");
        return $result;
    }
}

class Child extends FamilyMember
{	
}

class Mother extends FamilyMember
{
	public function loadTasks($file_name)
    {       
        
        if(!$this->tableExist('tasks')) $this->createNewTable('tasks');
        
        if($_FILES[$file_name]["error"] == UPLOAD_ERR_OK) {
            $lines = file($_FILES[$file_name]["tmp_name"]);
        }
        else {
            trigger_error("Error upload file", E_USER_ERROR);
            return false;
        }
        
        $this->mysqli->query("DELETE FROM tasks");
        
        foreach($lines as $line) {
                        
            $this->mysqli->query("INSERT INTO tasks (task) VALUES ('$line')");
        }
        trigger_error("file loaded succesful", E_USER_NOTICE);
        return true;
    }

}

class Father extends FamilyMember
{
    public function getAllTasks()
    {
        if (!$this->tableExist('tasks')) return false;
           
        $result = $this->mysqli->query("SELECT name, task, id FROM tasks");
        return $result;  
    }
    
    public function getAllNames()
    {
        if (!$this->tableExist('tasks')) return false;
           
        $result = $this->mysqli->query("SELECT name FROM users");
        return $result;  
    }
    
    public function distribute_tasks($data)
    {
        for ($i = 0; $i < count($data['user_name']); $i++) {
            $name = $data['user_name'][$i];
            $id = $data['task_id'][$i];
        
            $result = $this->mysqli->query("UPDATE tasks SET name = '$name' WHERE id = '$id'");
        }
        
        return $result;
    }

}

class Guest extends FamilyMember
{
    private $password;
    public function __construct($name, $password, $member="guest")
	{	        
        $this->password_hash = password_hash($password, PASSWORD_BCRYPT);   
        $this->password = $password;
        parent::__construct($name, $member);
    }   

    public function register()
    {                      
        if(!$this->tableExist('users')) $this->createNewTable('users');
            
        if ($this->userExist()){
            
            trigger_error("Name already exist", E_USER_NOTICE);
            return false;
        }
        
        $query = "INSERT INTO users VALUES( 'NULL' , '$this->name' , '$this->member', '$this->password_hash')";
        if(!$this->mysqli->query($query)){
            trigger_error("Unable to register user", E_USER_ERROR);
            return false;
        }
        return true;
    }

    public function login()
    {        
        if(!$this->tableExist('users')) return "No registered users";
        
        $result = $this->mysqli->query("SELECT * FROM users WHERE name = \"$this->name\"");
        
        $row = $result->fetch_assoc();
            
        if(!$row) return "Wrong name";
        else {
            if(password_verify($this->password, $row['password'])) {
                $_SESSION['name'] = $row['name'];
                $_SESSION['member'] = $row['member'];
                return "OK";
            }
            else return "Wrong password";
        }        
        
    }
}