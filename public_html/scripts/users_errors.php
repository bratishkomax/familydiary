<?php

function usersError($err_num, $err_msg, $err_file, $err_line)
{
    
    $error_str = "";
    
    switch ($err_num) {
        case E_USER_ERROR:
        case E_USER_WARNING:
        case E_USER_NOTICE:

            $error_str = <<<MYERROR
        <script>
            document.getElementById('errors').innerHTML = "$err_msg";
        </script>
MYERROR;
            break;

        default:
                 $error_str = <<<MYERROR
        <script>
            document.getElementById('errors').innerHTML = "Unknown error";
        </script>
MYERROR;
            break;
    }
    
    echo $error_str; 
    
    $dt = date("d-m-y h:m:s");
    error_log("[$dt] $err_msg\n", 3, "errors.log");

    
}
